# CEPC-ACTS test framework
This is the first version(v1) of CEPC detector implemented in the ACTS-framework
You can follow https://gitlab.cern.ch/acts/acts-framework/blob/master/README.md to build the project


In short:
```c++
git clone --recursive https://gitlab.cern.ch/jinz/acts-framework-cepc.git
cd acts-framework-cepc
source CI/setup_lcg94.sh
mkdir build
cd build
cmake -DACTS_BUILD_DD4HEP_PLUGIN=ON -DACTS_BUILD_EXAMPLES=ON -DACTS_BUILD_TGEO_PLUGIN=ON -DCMAKE_INSTALL_PREFIX=$PWD/../ -DUSE_DD4HEP=ON -DUSE_GEANT4=ON -DUSE_PYTHIA8=ON -DUSE_TGEO=ON -Ddfelibs_BUILD_EXAMPLES=ON -Ddfelibs_BUILD_UNITTESTS=ON -Ddfelibs_ENABLE_INSTALL=ON ..
make install -j4
```

## Building the environment
The environment could be built by:
```c++
cd acts-framework-cepc
source CI/setup_lcg94.sh
source bin/setup.sh
cd build/bin
```

## CEPC v0 Geometry 
The CEPC detector baseline design composed of seven parts:
`Beampipe, VTX, SIT, FTD, TPC, SET, ETD`
most of subdetectors were included in the v0 geometry except for `TPC`.
 Current CEPC tracker geometry (described in DD4HEP) could be find in `Detector/compact/`.


 `cepc_v0_master.xml` describes the world geometry in four sub-detectors, each consists of 
a Barrel region and an EndCap region:

*  `Tracker_1`: `VTX` + `FTD_N1` + `FTD_P1`
*  `Tracker_2`: `SIT_1` + `FTD_N2` + `FTD_P2`
*  `Tracker_3`: `SIT_2` + `FTD_N3` + `FTD_P3`
*  `ETD`: `SET` + `ETD_N` + `ETD_P`


The Geometry could be built with `ACTFWDD4hepGeometryExample`

``` c++
./ACTFWDD4hepGeometryExample --dd4hep-input ../../Detectors/DD4hepDetector/compact/CEPC/cepc_v04_master.xml --geo-subdetectors Tracker_1 Tracker_2 Tracker_3 ETD  --output-obj 1 --dd4hep-envelopeR 0.1 --dd4hep-envelopeZ 0.1
```

OBJ files generated for each sub-detectors could be viewed by any online 3D viewer


(e.g. https://www.3dvieweronline.com/index.php?route=account/login)

the output obj:
![Tracker_1_obj_](figures/Tracker1.png)
![Tracker_2_obj_](figures/Tracker2.png)
![Tracker_3_obj_](figures/Tracker3.png)
![ETD_obj](figures/Tracker4.png)

NOTE: in current master branch, `ACTFWDD4hepGeometryExample` can not terminate automatically, so you may need to use `control+C` to terminate after a while(one min is quite enough)


## Propagation 

 `PropagationExample` performs random test propagations throught the input detector geometry and allows to write them with certain debug informations.

 
 Following commands will generate 10 events (each contains substantial toy tracks) propagating through a magnetic field of 3 Tesla in z direction, recording the steps of different type (Runge kutta (type=1), navigation(type==0)  and  produecs a root output file with some default confiurations.

``` c++
./ACTFWDD4hepPropagationExample --dd4hep-input ../../Detectors/DD4hepDetector/compact/CEPC/cepc_v04_master.xml -n 10 --dd4hep-envelopeR 0.1 --dd4hep-envelopeZ 0.1 --bf-values 0 0 3 --output-root 1
```

The building of the tracking geometry in ACTS could be checked from the output root file:

X-Y view : Vertex Navigation detectors

![Vertex_navigation](figures/Vertex_navigation.png)

X-Y view: Vertex Sensitive detectors

![Vertex_sensitive](figures/Vertex_sensitive.png)

Z-R view: entire navigation detectors(w/o TPC)

![entire_navigation](figures/entire_navigation.png)


## GeantinoRecording 
ACTS allows the usage of Geant4 for (full detailed) detector material recording:
``` c++
./ACTFWGeantinoRecordingExample -n10 --dd4hep-input ../../Detectors/DD4hepDetector/compact/FCChhBaseline/FCChh_DectEmptyMaster.xml  --output-root true  -j 1
```

`-j 1` specifies to use one thread only to use Geant4

X-Y view : Beampipe and Vertex material

![materialXY](figures/materialXY.png)

total material

![materialXY](figures/materialtot.png)

## Material Mapping for OpenDataDetector 

Sinse material mapping is not implemented in the CEPC detector currently, the example is given with OpendataDetector in ACTS,

the following branch is (for the acts-framework) '158-create-dd4hepmaterialmapping' , 

and in the external/acts-core/, you should checkout branch  '642-translation-of-material-material-mapping-proxies-from-dd4hep-to-acts' to keep acts-framework and core in the consistent version.  In this branch, 

1. you can see the details of the ODD(OpenDataDetector) with DD4HEP : `geoPluginRun -input OpenDataDetector.xml -interactive -plugin DD4hep_GeometryDisplay -level 8`

2. you can construct the ACTS geometry: `./ACTFWDD4hepGeometryExample --dd4hep-input ../../Detectors/DD4hepDetector/compact/OpenDataDetector/OpenDataDetector.xml --geo-subdetectors BeamPipe Pixels ShortStrips LongStrips --output-obj 1`

3. run material mapping: 

(1) GeantinoRecording  `./ACTFWGeantinoRecordingExample -n100 --dd4hep-input ../../Detectors/DD4hepDetector/compact/OpenDataDetector/OpenDataDetector.xml --output-root true`

(2) MaterialMapping ` ./ACTFWDD4hepMaterialMappingExample -n100 --dd4hep-input=../../Detectors/DD4hepDetector/compact/OpenDataDetector/OpenDataDetector.xml -j1 --output-json true --mat-output-file opendatamaterial  --input-root true --input-files geant-material-tracks.root`

(3) MaterialValidation `./ACTFWDD4hepMaterialValidationExample -n100 --dd4hep-input=../../Detectors/DD4hepDetector/compact/OpenDataDetector/OpenDataDetector.xml --mat-input-type file --mat-input-file opendatamaterial_minmax.json  --output-root true`


## FATRAS
``` c++
./ACTFWDD4hepFatrasExample --dd4hep-input ../../Detectors/DD4hepDetector/compact/CEPC/cepc_v04_master.xml -n 10 --dd4hep-envelopeR 0.1 --dd4hep-envelopeZ 0.1 --bf-values 0 0 3 --output-root 1
```

## KalmanFilter 
Currently `KalmanFilter Fitting` Example is in the `kf fitting` branch, inserted in FATRAS. To run the Kalman filter you may need to switch to `kalman` branch:

``` c++
git checkout kalman
git submodule update
cd ../build
make install
./ACTFWDD4hepFatrasExample --dd4hep-input ../../Detectors/DD4hepDetector/compact/CEPC/cepc_v04_master.xml  --evg-input-type gun  --dd4hep-envelopeR 0.1 --dd4hep-envelopeZ 0.1 --evg-pileup 0 --pg-nparticles  10000 --pg-eta-range 1.3 1.3 --pg-phi-range 0 0  --pg-pt-range 10 10 --output-root 1 --fitted-tracks  test_tracks -n 1
```

The resolution of filtered(red) vs predicted(blue):

eLOC0:

![eLOC0](figures/res_loc0.png)

eLOC1:

![eLOC1](figures/res_loc1.png)

ePHI:

![ePHI](figures/res_phi.png)

eTHETA:

![eTHETA](figures/res_theta.png)


